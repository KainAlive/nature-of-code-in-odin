package main

import "core:fmt"
import "core:math/rand"
import "vendor:sdl2"
import "walker"

WINDOW_WIDTH :: 800
WINDOW_HEIGHT :: 600

STEP_SIZE :: 5

main :: proc() {
    shouldQuit: bool = false
	event: sdl2.Event

	sdl2.Init(sdl2.INIT_VIDEO)
    defer sdl2.Quit()

	window := sdl2.CreateWindow("Random Walk", sdl2.WINDOWPOS_CENTERED, sdl2.WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, nil)	
	defer sdl2.DestroyWindow(window)

	renderer := sdl2.CreateRenderer(window, -1, nil)
	defer sdl2.DestroyRenderer(renderer)

    w := walker.InitWalker(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2)

    for !shouldQuit {
        sdl2.PollEvent(&event)
		#partial switch event.type {
			case sdl2.EventType.QUIT:
				fmt.println("Quit")
				shouldQuit = true
        }
        
        // Render background
        sdl2.SetRenderDrawColor(renderer, 0, 0, 0, 0)
        sdl2.RenderClear(renderer)

        walker.DrawWalker(&w, renderer)

        xDir := rand.int_max(100) >= 50 ? 1 : -1
        yDir := rand.int_max(100) >= 50 ? 1 : -1

        if xDir == 1 do w.position.x += STEP_SIZE 
        if xDir == -1 do w.position.x -= STEP_SIZE
        if yDir == 1 do w.position.y += STEP_SIZE 
        if yDir == -1 do w.position.y -= STEP_SIZE 

        // Render out the render buffer
        sdl2.RenderPresent(renderer)
        sdl2.Delay(50)
    }
}
