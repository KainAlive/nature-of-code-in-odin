package walker

import "core:fmt"
import "vendor:sdl2"
import "../vector"

RECT_SIZE :: 10

Walker :: struct {
	position: vector.Vector2
}

past_positions: [dynamic]vector.Vector2

InitWalker :: proc(x, y: int) -> Walker {
	return Walker {
		position = vector.Vector2{
			x,
			y,
		}	
	}
}

DrawWalker :: proc(using w: ^Walker, r: ^sdl2.Renderer) {
	append(&past_positions, w.position)
	sdl2.SetRenderDrawColor(r, 255, 255, 255, 255)
	rect := sdl2.Rect{i32(position.x), i32(position.y), RECT_SIZE, RECT_SIZE}
	sdl2.RenderFillRect(r, &rect)
	if len(past_positions) <= 2 do return
	
	for i := 0; i < len(past_positions) - 1; i += 2 {
		x1 := i32(past_positions[i].x)
		y1 := i32(past_positions[i].y)
		x2 := i32(past_positions[i+1].x)
		y2 := i32(past_positions[i+1].y)
		sdl2.RenderDrawLine(r, x1, y1, x2, y2)
	}
}
