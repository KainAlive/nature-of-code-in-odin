package main

import "core:fmt"
import "vendor:sdl2"

WINDOW_WIDTH :: 400
WINDOW_HEIGHT :: 400

main :: proc() {
    shouldQuit: bool = false
	event: sdl2.Event

	sdl2.Init(sdl2.INIT_VIDEO)
    defer sdl2.Quit()

	window := sdl2.CreateWindow("Test", sdl2.WINDOWPOS_CENTERED, sdl2.WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, nil)	
	defer sdl2.DestroyWindow(window)

	renderer := sdl2.CreateRenderer(window, -1, nil)
	defer sdl2.DestroyRenderer(renderer)

    for !shouldQuit {
        sdl2.PollEvent(&event)
		#partial switch event.type {
			case sdl2.EventType.QUIT:
				fmt.println("Quit")
				shouldQuit = true
        }
        
        // Render background
        sdl2.SetRenderDrawColor(renderer, 0, 0, 0, 0)
        sdl2.RenderClear(renderer)
        
        // Render out the render buffer
        sdl2.RenderPresent(renderer)
    }
}
