# Nature of code in Odin

Based on [the book by Daniel Shiffman](https://natureofcode.com/book/) and adapted to the [Odin programming language](https://odin-lang.org/)
